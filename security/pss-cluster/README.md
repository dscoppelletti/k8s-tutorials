Apply Pod Security Standards at the Cluster Level
=================================================

Source: http://kubernetes.io/docs/tutorials/security/cluster-level-pss

## cluster-level-pss.yaml

Configuration file that can be consumed by the Pod Security Admission
Controller.

## cluster-config.yaml

Creates a cluster configuring the API server to consume the file `cluster-level-pss.yaml`.

## nginx-pod.yaml

Creates a Pod for a minimal configuration.
