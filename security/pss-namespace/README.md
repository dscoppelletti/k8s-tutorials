Apply Pod Security Standards at the Namespace Level
===================================================

Source: http://kubernetes.io/docs/tutorials/security/ns-level-pss

## nginx-pod.yaml

Creates a Pod for a minimal configuration.
