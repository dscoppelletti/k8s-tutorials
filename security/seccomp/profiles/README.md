seccomp profiles
================

## audit.json

Source: http://k8s.io/examples/pods/security/seccomp/profiles/audit.json

Allows all syscalls and logs them to syslog.

## violation.json

Source: http://k8s.io/examples/pods/security/seccomp/profiles/violation.json

Denies all syscalls.

## fine-grained.json

Source: http://k8s.io/examples/pods/security/seccomp/profiles/fine-grained.json

Allows a specified list of syscalls and denies the other ones.
