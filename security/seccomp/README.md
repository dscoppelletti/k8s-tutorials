Restrict a Container's Syscalls with seccomp
============================================

Source: http://kubernetes.io/docs/tutorials/security/seccomp

## kind.yaml

Source: http://k8s.io/examples/pods/security/seccomp/kind.yaml

Creates a single node cluster with the
[seccomp profiles](http://gitlab.com/dscoppelletti/k8s-tutorials/-/tree/master/security/seccomp/profiles) loaded but with feature `SeccompDefault` disabled.

## audit-pod.yaml

Source: http://k8s.io/examples/pods/security/seccomp/ga/audit-pod.yaml

Creates a Pod with a `seccomp` profile for syscall auditing.

## violation-pod.yaml

Source: http://k8s.io/examples/pods/security/seccomp/ga/violation-pod.yaml

Creates a Pod with a `seccomp` profile that causes violation.

## fine-pod.yaml

Source: http://k8s.io/examples/pods/security/seccomp/ga/fine-pod.yaml

Creates a Pod with a `seccomp` profile that only allows necessary syscalls.

## kind-seccomp.yaml

Creates a single node cluster with the with feature `SeccompDefault` enabled.

## default-pod.yaml

Source: http://k8s.io/examples/pods/security/seccomp/ga/default-pod.yaml

Creates a Pod that requests the `RuntimeDefault` `seccomp` profile for all its containers.
