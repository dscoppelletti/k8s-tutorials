Restrict a Container's Access to Resources with AppArmor
========================================================

Source: http://kubernetes.io/docs/tutorials/security/apparmor

## k8s-apparmor-example-deny-write.sh

Loads to all nodes a profile that denies all file writes.

## hello-apparmor.yaml

Simple Pod with the deny-write profile.

## hello-apparmor-2.yaml

Simple Pod with a profile that hasn't been loaded.
