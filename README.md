Kubernates Tutorials
====================

Source: http://kubernetes.io/docs/tutorials

This repository contains documentation assets reproduced or modified from
work created and shared by the Kubernetes Authors and used according to terms described in the [Creative Commons Attribution 4.0 International](http://creativecommons.org/licenses/by/4.0).
