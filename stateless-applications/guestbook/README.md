Example: Deploying PHP guestbook application with Redis
=======================================================

Source: http://kubernetes.io/docs/tutorials/stateless-application/guestbook

## redis-leader-deployment.yaml

Source: http://k8s.io/examples/application/guestbook/redis-leader-deployment.yaml

Creates a Deployment that runs a single replica Redis leader Pod.

## redis-leader-service.yaml

Source: http://k8s.io/examples/application/guestbook/redis-leader-service.yaml

Creates a Service to proxy the traffic to the Redis leader Pod.

## redis-follower-deployment.yaml

Source: http://k8s.io/examples/application/guestbook/redis-follower-deployment.yaml

Creates a Deployment that runs two Redis follower Pods.

## redis-follower-service.yaml

Source: http://k8s.io/examples/application/guestbook/redis-follower-service.yaml

Creates a Service to proxy the traffic to the Redis follower Pods.

## frontend-deployment.yaml

Source: http://k8s.io/examples/application/guestbook/frontend-deployment.yaml

Creates a Deployment that runs three frontend Pods.

## frontend-service.yaml

Source: http://k8s.io/examples/application/guestbook/frontend-service.yaml

Creates a Service to proxy the traffic to the frontend Pods.
