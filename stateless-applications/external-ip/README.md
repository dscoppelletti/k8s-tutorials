Exposing an External IP Address to Access an Application in a Cluster
=====================================================================

Source: http://kubernetes.io/docs/tutorials/stateless-application/expose-external-ip-address

## load-balancer-example.yaml

Source: http://k8s.io/examples/service/load-balancer-example.yaml

Creates a Deployment and an associated ReplicaSet. The ReplicaSet has five Pods
each of which runs the Hello World application.
