Configuring Redis using a ConfigMap
===================================

Source: http://kubernetes.io/docs/tutorials/configuration/configure-redis-using-configmap

## redis-pod.yaml

Source: http://raw.githubusercontent.com/kubernetes/website/main/content/enexamples/pods/config/redis-pod.yaml

Defines a [Redis](http://redis.io) Pod.

## example-redis-config-empty.yaml

Defines a ConfigMap with an empty configuration block.

## example-redis-config.yaml

Defines a ConfigMap with soome configuration values.
